import React from 'react';
import PropTypes from 'prop-types';
import Link from 'gatsby-link';
import styles from './styles.module.css';

const Header = ({ siteTitle, siteSlogan }) => (
  <div className={styles.header}>
    <div className={styles['header-layout']}>
      <h1 style={{ margin: 0 }}>
        <Link to="/" className={styles.logo}>
          {siteTitle}
        </Link>
      </h1>
      {siteSlogan}
    </div>
  </div>
);

Header.propTypes = {
  siteTitle: PropTypes.string.isRequired,
  siteSlogan: PropTypes.string.isRequired,
};

export default Header;
