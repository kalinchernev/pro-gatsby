import React from 'react';
import PropTypes from 'prop-types';
import Link from 'gatsby-link';

const Page = ({ data }) => {
  const posts = data.allMarkdownRemark.edges;

  return (
    <div>
      <p>Welcome to your new Gatsby site.</p>
      {posts.map(node => {
        const { frontmatter } = node.node;
        const { slug, title } = frontmatter;

        return (
          <Link to={slug}>
            <h3 key={slug}>{title}</h3>
          </Link>
        );
      })}
    </div>
  );
};

Page.propTypes = {
  data: PropTypes.shape({}).isRequired,
};

export default Page;

export const contentQuery = graphql`
  query getAllContent {
    allMarkdownRemark {
      edges {
        node {
          frontmatter {
            title
            slug
            date
          }
        }
      }
    }
  }
`;
